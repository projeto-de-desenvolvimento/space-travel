'use strict'

const User = use("App/Models/User")
const Trip = use("App/Models/Trip")

class TripController {

  async store ({ request, auth}) {
    const { id } = auth.user

    const data = request.only([
      'nickname',
      'latitude',
      'longitude',
      'address',
      'data_start',
      'data_end',
    ])

    const trip = await Trip.create({ ...data, user_id: id })

    return trip

  }

  async show ({ params }) {
    const trip = await Trip.findOrFail(params.id)

    await trip.load('backpacks')

    await trip.load('places')

    return trip
  }

  async update ({ params, request }) {
    const trip = await Trip.findOrFail(params.id)

    const data = request.only([
      'nickname',
      'latitude',
      'longitude',
      'address',
      'date_start',
      'date_end',
      'temperature'
    ])

    trip.merge(data)

    await trip.save()

    return trip
  }

  async destroy ({ params, auth, response }) {
    const trip = await Trip.findOrFail(params.id)

    if (trip.user_id !== auth.user.id) {
      return response.status(401).send({ error: 'Você não possui permissão!'})
    }

    await trip.delete()
  }
}

module.exports = TripController
