'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TripSchema extends Schema {
  up () {
    this.create('trips', (table) => {
      table.increments()
      table.string('nickname').notNullable()
      table.decimal('latitude', 9, 6).notNullable()
      table.decimal('longitude', 9, 6).notNullable()
      table.string('address').notNullable()
      table.date('date_start').notNullable()
      table.date('date_end').notNullable()
      table.integer('temperature', 2)
      table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('trips')
  }
}

module.exports = TripSchema
