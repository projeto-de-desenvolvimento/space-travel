'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PersonalInfoSchema extends Schema {
  up () {
    this.create('personal_infos', (table) => {
      table.increments()
      table.string('age', 2)
      table.string('phone', 14)
      table.string('address', 80)
      table.string('cep', 8)
      table.string('food_1', 30)
      table.string('food_2', 30)
      table.string('food_3', 30)
      table.string('music_1', 30)
      table.string('music_2', 30)
      table.string('music_3', 30)
      table.string('local_1', 30)
      table.string('local_2', 30)
      table.string('local_3', 30)
      table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('personal_infos')
  }
}

module.exports = PersonalInfoSchema
