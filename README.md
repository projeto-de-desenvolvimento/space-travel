# Space Travel

<h4>Aplicativo de Auxilio de viagem<h4>

<a href="#prot">Prototipo</a><br>
<a href="#aplicativo">Aplicativo</a><br>
<a href="#demo-app">Demostração do App</a><br>
<a href="#tecnologias-app">Tecnologias do App</a><br>
<a href="#api">A Api</a><br>
<a href="#demo-api">Demostração da API</a><br>
<a href="#tec-passos">Tecnologias da API</a><br>

<h2 id="prot">Prototipos</h2>

![Prototipos](https://i.imgur.com/RNld8bE.png)

As telas que serão apresentadas no aplicativo podem ser acessadas atraves do seguinte link

**Link:** [Figma](https://www.figma.com/proto/Dzx8F6WvxQ2LyHeQVyHQuADU/Space-Travel?node-id=173%3A157&scaling=scale-down)

<h2 id="aplicativo">Aplicativo</h2>

O Projeto Space Travel tem como objetivo criar um aplicativo hibrido para auxilio e organização de viagem, de forma efetiva e simples. 

Ele permite que o usuario, adicionar onde deseja viajar, organizar suas datas de viagem.

Apresenta de acordo com as preferencias pessaois, lugarges para visitar durante a estadia, com opções de malas a serem feitas de acordo com o clima e a temperatura do local visitado.

<h4 id="demo-app">Demostração</h4>


**Abaixo algumas informações para a demonstração do app**

**Link:** [Snack(expo)](https://snack.expo.io/@thaua97/github.com-thaua97-space-travel-app)

**Credenciais:** Você pode criar seu proprio usuario para efetuar o acesso a aplicação!


<h1 id="tecnologias-app">Tecnologias do app</h1>

<h4>[React Native](https://facebook.github.io/react-native/)</h4>

O React Native é um projeto desenvolvido pelos engenheiros do Facebook e que consiste em uma série de ferramentas que viabilizam a criação de aplicações móveis nativas para a plataforma iOS e Android, utilizando o que há de mais moderno no desenvolvimento Front-end – mirando no futuro. É o estado da arte no que se refere ao desenvolvimento mobile baseado em JavaScript.

O stack do React Native é poderoso, pois nos permite utilizar ECMAScript 6, CSS Flexbox, JSX, diversos pacotes do NPM e muito mais. Sem contar que nos permite fazer debug na mesma IDE utilizada para o desenvolvimento nativo com essas plataformas (além de tornar o processo extremamente divertido).

**Para elaboração do projeto estão sendo utilizadas as seguintes dependencias!**

* axios: ^0.19.0
* expo: ^33.0.0
* prop-types: ^15.7.2
* react: 16.8.3
* react-dom: ^16.8.6
* react-icons: ^3.7.0
* react-native: ^0.59.1
* react-native-image-overlay: ^0.1.2
* react-native-linear-gradient: ^2.5.4
* react-navigation: ^3.11.0
* styled-components: ^4.3.2

**Criando um app**

Acesse o seguinte artigo e siga os seguintes passos

**link:** [artigo rocketseat](https://docs.rocketseat.dev/ambiente-react-native/introducao)

ou 

```bash
    
    npm install expo-cli --global
    
    expo init my-new-project
    
    cd my-new-project
    
    expo start
```


<h1 id="api">API</h1>

<h4>O que é uma API?</h4>

API é um conjunto de rotinas e padrões de programação para acesso a um aplicativo de software ou plataforma baseado na Web. A sigla API refere-se ao termo em inglês "Application Programming Interface" que significa em tradução para o português "Interface de Programação de Aplicativos"

A API principal desta aplicação foi construida através do framework AdonisJS. Também esta sendo utilizada a API climatempo, disponibiliza a previsão do tempo e outros dados meteorológicos em tempo real.

<h4 id="demo-api">Demostração</h4>

A API esta hospedada no serviço [Heroku](https://dashboard.heroku.com/) e pode ser acessada atraves das inforamções abaixo.

**Link API:** [https://space-travel-server.herokuapp.com](https://space-travel-server.herokuapp.com)

**Rotas:** [Arquivo das rotas](https://drive.google.com/file/d/1VwQEwH4UPpHYPnDQmqBQh5Kd74TWGTRD/view?usp=sharing)

<h4 id="tec-passos">Tecnologias e passo a passo</h4>

**Estes são os passos para criar uma API em AdonisJS, a mesma vem pré-configurada com.**

1. Bodyparser
2. Authenticação
3. CORS
4. Lucid ORM
5. Migrations e seeds

## Setup

Use o comando adonis para instalar o blueprint

```bash

npm i -g @adonisjs/cli

adonis new myserver --api-only

cd myserver

adonis serve

```

ou faça um clone do repo e execute o comando `npm install` ou `yarn install`.


### Migrations

Para rodar as migrations siga os seguntes passos.

```js
adonis migration:run
```
